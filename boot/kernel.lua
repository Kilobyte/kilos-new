--[[
    KilOS Kernel main file
    Provides everything kilos needs to run
]]
KILOS_VERSION = "3.0.1"
local logscreenid, modemside = 7, "right"

function log(message)
    if logscreenid then 
        rednet.open("right")
        rednet.send(logscreenid, message)
    end
end
rawset(_G, "log", log)

local kernelsbx = getfenv(0)
function run()
    print("KilOS "..KILOS_VERSION.." booting...")
    -- First of all we need to do stuff like loading modules
    write("Loading Kernel modules... ")
    if not vfs.exists("/boot/mod") then
        -- shouldn't happen, but apperently it did
        vfs.makeDir("/boot/mod")
    elseif not vfs.isDir("/boot/mod") then
        -- uhhhhh... thats bad
        error("Could not load Kernel modules: /bin/mod is not a directory")
    end

    local initlist = {}
    function onInit(callback)
        table.insert(initlist, callback)
    end
    
    rawset(_G, "onInit", onInit)

    local modlist = vfs.list("/boot/mod")
    for k, v in ipairs(modlist) do
        local n = vfs.combine("/boot/mod", v)
        if vfs.isFile(n) then
            local f, m = vfs.loadfile(n)
            if not f then error(m) end
            setfenv(f, kernelsbx)
            f()
        end
    end
    print("Done. "..#modlist.." Modules found.")

    local initproc = false
    function kernel_createAndRunThread(func)
        --print(initproc)
        if not initproc then 
            process_create(func, nil, "kernel", 0, nil, nil, nil, nil, "init")
            initproc = true
        else
            local t = thread_create(func, 0)
            thread_resume(t)
        end
    end
    rawset(_G, "kernel_createAndRunThread", kernel_createAndRunThread)

    write("Initializing Kernel Modules... ")
    -- now init the modules that want to be initialized
    for k, v in ipairs(initlist) do
        v()
    end
    print("Done.")

    local function startShell()
        print("Launching shell...")
        local s, m = pcall(shell_launch, "/bin/ksh.lua")
        if not s then printError("Failed to launch shell: "..m) end
    end
    kernel_createAndRunThread(startShell)

    -- cleanup
    initlist = nil
    onInit = nil

    -- ==We are now ready to launch stuff==

    -- start daemons

    --shell_launch("/test.lua")

    -- load frontend. if theres none continue on console
    print("Starting task scheduler...")
    thread_startEngine()
    error("Thread engine stopped somehow")
end

local s, m = pcall(run)
if not s then 
    printError("Kernel Panic: "..(m or "Unknown Error :/"))
    printError("Please report on KilOS issue tracker and reboot")
    while true do
        os.pullEventRaw()
    end
end