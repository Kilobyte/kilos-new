local function onTermEvent(meta, ...)
    local name = meta.name:sub(8)
    term_termEvent(term, name, ...)
end    

function shell_launch(shell, ...)
    local f, n = loadfile(shell)
    if not f then error(n) end
    local pid1, tid1 = process_create(f, 0, "user", nil, nil, nil, term, nil, fs.getName(shell))
    --print("bam")
    event_addHandler({
            "native_mouse_click",
            "native_mouse_drag",
            "native_mouse_scroll",
            "native_char",
            "native_key"},
        onTermEvent)
end