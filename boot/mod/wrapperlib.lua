local function split(self, sep)
        local sep, fields = sep or ":", {}
        local pattern = string.format("([^%s]+)", sep)
        self:gsub(pattern, function(c) fields[#fields+1] = c end)
        return fields
end
rawset(string, "split", split)

local function init()
    for k, v in pairs(string) do
        syscall_add("string_"..k, function (tid, pid, ...)
            return v(...)
        end)
    end

    for k, v in pairs(table) do
        syscall_add("table_"..k, function (tid, pid, ...)
            return v(...)
        end)
    end

    syscall_add("lua_unpack", function (tid, pid, ...)
        return unpack(...)
    end)

    syscall_add("lua_pairs", function (tid, pid, ...)
        return pairs(...)
    end)

    syscall_add("lua_next", function (tid, pid, ...)
        return next(...)
    end)

    syscall_add("lua_tostring", function (tid, pid, ...)
        return tostring(...)
    end)

    syscall_add("lua_tonumber", function (tid, pid, ...)
        return tonumber(...)
    end)

    syscall_add("lua_type", function (tid, pid, ...)
        return type(...)
    end)

    --[[for k, v in pairs(term) do
        syscall_add("term_"..k, function (tid, pid, ...)
            return v(...)
        end)
    end]]
end
onInit(init)