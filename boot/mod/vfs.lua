vfs.loadlib("json")
if not vfs.exists("/etc/fsperms.json") then vfs.writeAll("/etc/fsperms.json", "{}") end
local files = {}

local function getPerms(uid, file)
    --if not filter then filter = {r = true, w = true, x = true}
    -- load perms
    local perms = json.decode(vfs.readAll("/etc/fsperms.json")) -- temporary till i get a proper binary format
    for path in ipath(file) do
        local p = perms[file]
        if not p then 
            p = {uid = 0, u = {r = true, w = true, x = false}, u = {r = true, w = false, x = false}, u = {r = true, w = false, x = false}}
            perms[file] = p
            vfs.writeAll("/etc/fsperms.json", json.encode(perms))
        end
        if p.uid == uid then
            -- owner/user
            return p.u -- u instead of users to save memory
        else
            return p.o -- o instead of other to save memory
        end
    end
end

local function getPermsByPid(pid, file)
    return getPerms(process_get(pid).uid, file)
end

local function setPerms(path, perms)
    local perms_ = json.decode(vfs.readAll("/etc/fsperms.json"))
    perms_[path] = perms
    vfs.writeAll("/etc/fsperms.json", json.encode(perms_))
end

local function init()
    syscall_add("vfs_list", function (tid, pid, dir)
        if not getPermsByPid(pid, dir).r then
            error("Access denied")
        end
        return vfs.list(dir)
    end)
    syscall_add("vfs_open", function (tid, pid, path, mode)
        if not mode then mode = "r" end
        if mode:find("r") then
            if not getPermsByPid(pid, path).r == true then -- == because it can also be nil and that needs different handling
                error("Access denied")
            end
        end
        if mode:find("w") or mode:find("a") or not mode:find("r") then
            if (not vfs.exists(path)) and vfs.exists(vfs.combine(path, "..")) then
                if not getPermsByPid(pid, vfs.combine(path, "..")).w then
                    error("Access denied")
                end
                setPerms(path, {uid = process_get(pid).uid, u = {r = true, w = true, x = false}, g = {r = true, w = false, x = false}, o = {r = true, w = false, x = false}})
            end
            if not getPermsByPid(pid, dir).w then
                error("Access denied")
            end
        end
        local handle = vfs.open(path, mode)
        --print(path)
        if handle == nil then return nil, "fs.open failed" end
        local h = math.random(1, 999999)
        files[h] = handle
        return h
    end) 
    syscall_add("vfs_write", function (tid, pid, handle, data)
        files[handle].write(data)
    end)
    syscall_add("vfs_readAll", function (tid, pid, handle)
        utils_checkType(handle, "number", 1, 5, "syscall/vfs_readAll")
        return files[handle].readAll()
    end)
    syscall_add("vfs_readLine", function (tid, pid, handle)
        return files[handle].readLine()
    end)
    syscall_add("vfs_close", function (tid, pid, handle)
        files[handle].close()
    end)    
    syscall_add("vfs_exists", function (tid, pid, path)
        if not getPermsByPid(pid, vfs.combine(dir, "..")).r then
            error("Access denied")
        end
        return vfs.exists(path)
    end)
    syscall_add("vfs_combine", function (tid, pid, base, rel)
        return vfs.combine(base, rel)
    end)
    syscall_add("vfs_native", function (tid, pid, name, ...)
        vfs[name](...)
    end)
    syscall_add("lua_loadstring", function (tid, pid, string, chunkname)
        utils_checkType(string, "string", 1, 5, "syscall/lua_loadstring")
        utils_checkType(chunkname, {"string", "nil"}, 2, 5, "syscall/lua_loadstring")
        if process_get(pid).uid ~= 0 and string:sub(1,1):byte() == 27 then
            error("Only root (UID 0) may load bytecode", 3)
        end
        local f, m = loadstring(string, chunkname)
        if f then setfenv(f, process_get(pid).env) end
        return f, m
    end)
    syscall_add("vfs_getFilePerms", function (tid, pid, file)
        return getPermsByPid(pid, file)
    end)
    process_onEnvSetup(function(uid, pid, env) 
        --[[function env.include(lib)
            local f, m = vfs.loadfile("/lib/"..lib..".lua") -- VERY TEMPORARY
            if not f then error(m, 2) end
            setfenv(f, env)
            f()
        end]]
        local f, m = vfs.loadfile("/lib/initsbx.lua")
        if not f then error(m, 2) end
        setfenv(f, env)
        f()
    end)
end

onInit(init)