--[[
    Manages Syscalls
]]

local syscalls = {}

function syscall_add(name, handler)
    syscalls[name] = handler
end

function syscall_remove(name)
    handler = nil
end

function syscall_perform( thread, pid, name, ...)
    --print("syscall: ", name, ...)
    if thread_shallKill(thread) then
        error("Thread Killed", 1)
    end
    if not syscalls[name] then
        error("Invalid syscall: "..name, 3)
    end
    return syscalls[name](thread, pid, ...)
end