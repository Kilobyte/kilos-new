envvars = {}
local function init()
    syscall_add("print", function (tid, pid, ...)
        print(...)
    end)

    syscall_add("io_stdout", function (tid, pid, text)
        process_set(pid, "stdoutbuff", process_get(pid).stdoutbuff..text)
        --print("pid of writer: ", pid)
    end)

    syscall_add("io_stdin", function (tid, pid, mode, block)
        local function dostuff()
            local inp = process_get(pid).stdinbuff
            if mode == "line" then
                local ret = ""
                while true do
                    if 1 == inp:len() and not block then
                        ret = ret..inp
                        inp = ""
                        return ret
                    elseif inp:len() == 0 and not block then
                        return ret
                    elseif inp:len() == 0 then
                        coroutine.yield()
                    end
                    ret = ret.inp:sub(1,1)
                    inp = inp:sub(2)
                    if ret:sub(-1, -1) == "\n" then
                        return ret:sub(1, -2)
                    end
                end
            elseif mode == "char" then
                local ret
                while inp:len() == 0 do
                    ret = inp:sub(1,1)
                    inp = ret.inp:sub(1,1)
                    if not block then break end
                    coroutine.yield()
                end
                return ret
            end
        end
        local ret = dostuff()
        --if ret:sub(-2, -1) == "\n" then ret = ret:sub(1, -3) end
        return ret
    end)

    syscall_add("io_stderr", function (tid, pid, text)
        process_set(pid, "stderrbuff", process_get(pid).stderrbuff..text)
    end)

    syscall_add("io_readstderr", function (tid, pid, tarpid, mode)
        if not process_mayInteractWith(pid, tarpid) then
            error("Cannot read stderr from that process", 3)
        end
        local inp = process_get(tarpid).stderrbuff
        if mode == "line" then
            local ret = ""
            while true do
                if 1 == inp:len() then
                    ret = ret..inp
                    inp = ""
                    return ret
                elseif inp:len() == 0 then
                    return ret
                end
                ret = ret.inp:sub(1,1)
                inp = inp:sub(2)
                if ret:sub(-1, -1) == "\n" then
                    return ret:sub(1, -2)
                end
            end
        elseif mode == "char" then
            local ret = inp:sub(1,1)
            inp = ret.inp:sub(1,1)
            return ret
        end
    end)

    syscall_add("io_readstdout", function (tid, pid, tarpid, mode)
        if not process_mayInteractWith(pid, tarpid) then
            error("Cannot read stdout from that process", 3)
        end
        --print("1(read from):", tarpid or "nil")
        local inp = process_get(tarpid).stdoutbuff
        if mode == "line" then
            --print("2")
            local ret = ""
            while true do
                --print("3:", ret or "nil", ":", inp or "nil")
                if 1 == inp:len() then
                    ret = ret..inp
                    inp = ""
                    process_set(tarpid, "stdoutbuff", inp)
                    return ret
                elseif inp:len() == 0 then
                    process_set(tarpid, "stdoutbuff", inp)
                    return ret
                end
                ret = ret..inp:sub(1,1)
                inp = inp:sub(2)
                if ret:sub(-1, -1) == "\n" then
                    process_set(tarpid, "stdoutbuff", inp)
                    return ret:sub(1, -2)
                end
            end
        elseif mode == "char" then
            local ret = inp:sub(1,1)
            inp = ret.inp:sub(1,1)
            process_set(tarpid, "stdoutbuff", inp)
            return ret
        end
    end)

    syscall_add("io_writestdin", function (tid, pid, tarpid, mode)
        if not process_mayInteractWith(pid, tarpid) then
            error("Cannot write stdin of that process", 3)
        end
        process_set(tarpid, "stdinbuff", process_get(tarpid).stdinbuff..text)
    end)

    syscall_add("yield", function (tid, pid, ...)
        return coroutine.yield(...)
    end)

    syscall_add("mt_get", function (tid, pid, tab)
        --[[local mt = getmetatable(tab)
        if mt.__KilOS and mt.__KilOS.__mt then
            return mt.__KilOS.__metatable
        end
        if not mt then return nil end
        local mtcopy = utils_copyTable(tab)
        mtcopy.__KilOS = nil
        mtcopy.__metatable = mt.__KilOS.__mt
        if mt.__KilOS.fakenil then
            return nil
        end
        return mtcopy]]
        return getmetatable(tab)
    end)

    syscall_add("mt_set", function (tid, pid, tab, mt)
        --[[if mt and mt.__KilOS then
            error("metatables may not contain an __KilOS entry. It is reserved for the Kernel")
        end
        --mt.__KilOS = (getmetatable(tab) or {}).__KilOS
        local kos = (getmetatable(tab) or {}).__KilOS
        if kos.__metatable then
            error("Cannot modify protected metatable", 3)
        end
        local mt2 = utils_copyTable(mt)
        if kos and not mt then
            kos.fakenil = true
            kos.__metatable = false
            mt2 = {}
        elseif kos and kos.fakenil and mt then
            kos.fakenil = nil
        end
        if mt.__metatable then
            mt2.__metatable = nil
            kos = kos or {}
            kos.__mt = mt.__metatable
        end
        mt2.__KilOS = kos
        setmetatable(tab, mt2)
        return tab]]
        return setmetatable(tab, mt)
    end)

    syscall_add("env_getglob", function (tid, pid, name)
        utils_checkType(name, "string", 1, 3, "syscall/env_getglob")
        return envvars[name]
    end)

    syscall_add("env_setglob", function (tid, pid, name, value)
        if process_get(pid).uid ~= 0 then
            error("Access denied", 3)
        end
        utils_checkType(name, "string", 1, 3, "syscall/env_setglob")
        envvars[name] = utils_checkType(value, {"string", "nil"}, 2, 3, "syscall/env_setglob")
    end)

    syscall_add("lua_error", function (tid, pid, msg, level)
        if level then level = level + 5 else level = 5 end
        error(msg, level)
    end)

    process_onEnvSetup(function (uid, pid, env)
        function env.syscall(name, ...)
            utils_checkType(name, "string", 1, 2, "native/syscall")
            --local ret = {}
            --local function t(name, ...)
                --print("Running thread: ", thread_getRunning() or "nil", " data: ", thread_get(thread_getRunning()) or "nil")
                --if name == "term" then print(name, ...) end
                return syscall_perform(thread_getRunning(), thread_get(thread_getRunning()).pid, name, ...)
            --end
            --[[setfenv(t, getfenv())
            local s = pcall(t, name, ...)
            if not s then error("Internal error on syscall/"..name..": "..(m or "Unknown")) end
            return unpack(ret)]]
        end
        --print("syscall registered")
    end)
end

onInit(init)