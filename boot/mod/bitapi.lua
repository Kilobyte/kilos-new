local function init()
    syscall_add("bit_tobits", function (tid, pid, ...)
        return bit.tobits(...)
    end)

    syscall_add("bit_lshift", function (tid, pid, ...)
        return bit.blshift(...)
    end)

    syscall_add("bit_rshift", function (tid, pid, ...)
        return bit.brshift(...)
    end)

    syscall_add("bit_xor", function (tid, pid, ...)
        return bit.bxor(...)
    end)

    syscall_add("bit_or", function (tid, pid, ...)
        return bit.bor(...)
    end)

    syscall_add("bit_and", function (tid, pid, ...)
        return bit.band(...)
    end)

    syscall_add("bit_not", function (tid, pid, ...)
        return bit.bnot(...)
    end)

    syscall_add("bit_tonum", function (tid, pid, ...)
        return bit.tonumb(...)
    end)
end

onInit(init)