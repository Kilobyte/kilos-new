--[[
    IO Lib
    Provides functions like Print and read
]]
include "streams"
include "metatables"

---------- Class stdout ----------

local Stdout = {}

-- constructor
local function Stdout_new()
    local t = setmetatable({}, Stdout)
    return t
end

function Stdout.write(...)
    for k, v in ipairs({...}) do
        syscall("io_stdout", tostring(v))
    end
end

setmetatable(stdout, {__index=streams.OutputStream, __call=Stdout_new})

---------- End of class stdout ----------

---------- Class Stderr ----------

local Stderr = {}

-- constructor
local function Stderr_new()
    local t = setmetatable({}, Stderr)
    return t
end
setmetatable(Stderr, {__index=Stderr, __call=Stderr_new})

function Stderr:write(...)
    for k, v in ipairs({...}) do
        syscall("io_stderr", tostring(v))
    end
end

---------- End of class Stderr ----------


---------- Class Stdin ----------

local Stdin = {}

-- constructor
local function Stdin_new()
    local t = setmetatable({}, Stdin)
    return t
end

function Stdin:read(block)
    if block == nil then block = true
    return syscall("io_stdin", "char", block)
end

function Stdin:readLine(block)
    if block == nil then block = true
    return syscall("io_stdin", "line", block)
end

setmetatable(Stdin, {__index=streams.InputStream, __call=Stdin_new})

---------- End of class Stdin ----------

stdout = Stdout()
stdin = Stdin()
stderr = Stderr()

--[[function print(...)
    write(...)
    syscall("io_stdout", "\n")
end

function write(...)
    for k, v in ipairs({...}) do
        syscall("io_stdout", tostring(v))
    end
end

function printError(...)
    writeError(...)
    syscall("io_stderr", "\n")
end

function writeError(...)
    for k, v in ipairs({...}) do
        syscall("io_stderr", tostring(v))
    end
end

function readChar()
    return syscall("io_stdin", "char", true)
end

function readLine(hide, mask)
    return syscall("io_stdin", "line", true)
end]]
