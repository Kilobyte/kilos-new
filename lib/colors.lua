include "table"

-- Colors
colors = {
  white = 1,
  orange = 2,
  magenta = 4,
  lightBlue = 8,
  yellow = 16,
  lime = 32,
  pink = 64,
  gray = 128,
  lightGray = 256,
  cyan = 512,
  purple = 1024,
  blue = 2048,
  brown = 4096,
  green = 8192,
  red = 16384,
  black = 32768
}

function colors.combine( ... )
  local r = 0
  for n,c in ipairs( { ... } ) do
    r = bit.bor(r,c)
  end
  return r
end

function colors.subtract( colors, ... )
	local r = colors
	for n,c in ipairs( { ... } ) do
		r = bit.band(r, bit.bnot(c))
	end
	return r
end

function colors.test( colors, color )
  return ((bit.band(colors, color)) == color)
end

colours = {}
for k, v in pairs(colors) do
  colours[k] = v
end

colours.gray = nil
colours.grey = colors.gray

colours.lightGray = nil
colours.lightGrey = colors.lightGray
