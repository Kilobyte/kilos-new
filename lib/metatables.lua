function setmetatable(tab, ...)
    syscall("mt_set", tab, ...)
    return tab
end

function getmetatable(...)
    return syscall("mt_get", ...)
end