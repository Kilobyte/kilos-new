include "metatables"
include "utils"

local files = setmetatable({}, {__mode = "k"})
io = {}

---------- Class io.File ----------

io.File = {__index = io.File}

-- constructor
local function File_new(t_, filename)
    utils.checkType(filename, "string", 1, 3, "File_new")
    local t = setmetatable({}, io.File)
    files[t] = {name = filename}
    --for k, v in pairs(getmetatable(t)) do write(k..", ") end print()
    return t
end
setmetatable(io.File, {__call=File_new})

function io.File.new(...)
    return File_new(nil, ...)
end

function io.File:open(mode)
   --local perms = syscall("getFilePerms")
   files[self].handle = syscall("vfs_open", files[self].name, mode or "r")
   return self -- Allows for chaining like io.File("test.txt"):open("w"):writeLine("blah"):close()
end

function io.File:write(text)
    if not files[self].handle then 
        error("Not opened file")
    end
    syscall("vfs_write", files[self].handle)
end

function io.File:writeLine(text)
    self:write(text.."\n")
end

function io.File:readAll()
    if not files[self].handle then 
        error("Not opened file")
    end
    return syscall("vfs_readAll", files[self].handle)
end

function io.File:readLine()
    if not files[self].handle then 
        error("Not opened file")
    end
    return syscall("vfs_readLine", files[self].handle)
end

function io.File:close()
    if not files[self].handle then 
        error("Not opened file")
    end
    syscall("vfs_close", files[self].handle)
    files[self].handle = nil
end

function io.File:exists()
    return syscall("vfs_exists", files[self].name)
end

function io.File:getName()
    return io.getName(files[self].name)
end

---------- End of class io.File ----------

function io.getName(file)
    local s = file:split("/")
    if s[#s] ~= "" or #s == 1 then return s[#s] else return s[#s - 1] end
end

function io.combine(base, rel)
    return syscall("vfs_combine", base, rel)
end