include "string"
include "table"

utils = {}
function utils.checkType(obj, t, paramnr, stacklvl, name)
    if type(t) == "string" then
        if type(obj) == t then
            return obj
        else
            error((name and (name..": ") or "")..(paramnr and ("Parameter #"..paramnr..": ") or "")..t.." expected, got "..type(obj), stacklvl or 2)
        end
    elseif  type(t) == "table" then
        if utils.getkey(t, type(obj)) then
            return obj
        else
            local function callback(num, k, v)
                if k == num then
                    return " or "
                elseif k > 1 then
                    return ", "
                end
            end
            error((paramnr and ("Parameter #"..paramnr..": ") or "")..utils_joinTable(t, callback).." expected, got "..type(obj), stacklvl or 2)
        end
    end
end

function type(...)
    return syscall("lua_type", ...)
end

function utils.getKey(haystack, needle)
    for k, v in pairs(haystack) do
        if v == needle then return k end
    end
end