user = {}

function user.getName(id)
    if not id then
        return "root"
    end
end

function user.getID(name)
    if not name then
        return 0
    end
end

function user.getHome(id)
    if type(id) == "string" then id = user.getID(id) end
    if not id then
        return "/root"
    end
end