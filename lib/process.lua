include "metatables"
include "table"
process = {}

---------- Class process.Process   ----------

process.Process = {}

-- constructor
local function Process_new(func)
    local t = setmetatable({func = func}, process.Process)
    return t
end
setmetatable(process.Process, {__index=process.Process, __call=Process_new})

function process.Process:setTerm(term)
    self.term = term
    return self
end

function process.Process:run(...)
    local pid = syscall("process_create", self.func, {...}, self.term)
    self.pid = pid
    return self
end

function process.Process:send(...)
    syscall("process_send", self.pid, ...)
    return self
end

---------- End of class process.Process ----------
