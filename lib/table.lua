function unpack(...)
    return syscall("lua_unpack", ...)
end

-- Credits @ http://lua-users.org/wiki/IteratorsTutorial :)
function ipairs(...)
    local t = {...}
    local tmp = {...}
    -- if nothing to iterate over just return a dummy iterator
    if #tmp==0 then
        return function() end, nil, nil
    end
    local function mult_ipairs_it(t, i)
        i = i+1
        for j=1,#t do
            local val = t[j][i]
            if val == nil then return val end
            tmp[j] = val
        end
        return i, unpack(tmp)
    end
    return mult_ipairs_it, t, 0
end

function pairs(...)
    return syscall("lua_pairs", ...)
end

function next(...)
    return syscall("lua_next", ...)
end

local lib = {}

table = {"maxn", "foreachi", "concat", "remove", "insert", "foreach", "sort", "getn"}
for k, v in ipairs(lib) do
    local v_ = v
    table[v_] = function(...)
        return syscall("table_"..v_, ...)
    end
end 