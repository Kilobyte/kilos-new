include "io"
include "env"
include "user"
include "utils"
include "string"

path = {}

function path.resolveLib(rel)
    utils.checkType(rel, "string", 1, 3)
    if string.startsWith(rel, "/") then
        if io.File(rel):exists() then
            return rel
        else
            return nil
        end
    elseif rel:match("^%~%a-%/") then
        local pre, name, post, path = rel:match("^(%~)(%a-)(%/)(.*)$")
        if name == "" then name = nil end
        return io.combine(user.getHome(name), path)
    elseif rel:find("/") then
        local ret = io.combine(env.getVar("dir"), rel)
        if io.File(ret):exists() then
            return ret
        else
            return nil
        end
    elseif string.startsWith(rel, "./") then
        local ret = io.combine(env.getVar("dir"), rel)
        if io.File(ret):exists() then
            return ret
        else
            return nil
        end
    end
    for p in env.getVar("libpath"):gmatch("[^:]+") do
        local f = io.combine(p, rel)
        print(f)
        if io.File.exists(io.File(f)) then --io.File(io.combine(p, rel)):exists()
            return f
        end
    end
end

function path.resolve(rel)
    utils.checkType(rel, "string", 1, 3)
    if string.startsWith(rel, "/") then
        if io.File.exists(io.File.new(rel)) then
            return rel
        else
            return nil
        end
    elseif rel:match("^%~%a-%/") then
        local pre, name, post, path = rel:match("^(%~)(%a-)(%/)(.*)$")
        if name == "" then name = nil end
        return io.combine(user.getHome(name), path)
    elseif string.startsWith(rel, "./") then
        local ret = io.combine(env.getVar("dir"), rel)
        if io.File.exists(io.File.new(ret)) then
            return ret
        else
            return nil
        end
    elseif rel:find("/") then
        local ret = io.combine(env.getVar("dir"), rel)
        print(ret)
        if io.File.exists(io.File.new(ret)) then
            return ret
        else
            return nil
        end
    end
    for p in env.getVar("path"):gmatch("[^:]+") do
        local f = io.combine(p, rel)
        --print(f)
        if io.File.exists(io.File.new(f)) then --io.File(io.combine(p, rel)):exists()
            return f
        end
    end
end