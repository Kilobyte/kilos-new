--[[
    Wrapper for event syscalls
]]

include "table"

event = {}
function event.addHandler(...)
    return syscall("process_addListener", ...) -- it doesn't return stuff atm, but it won't hurt to add a return either
end

function event.addListener(...)
    return event.addHandler(...)
end

function event.waitFor(...)
    local event = nil
    local function onEvent(...)
        event = {...}
    end
    if #{...} == 0 then return end
    for k, v in ipairs({...}) do
        syscall("process_addListener", v, onEvent)
    end
    while true do
        syscall("yield")
        if event then 
            for k, v in ipairs({...}) do
                syscall("process_removeListener", v, onEvent)
            end
            return unpack(event)
        end
    end
end

function event.removeListener(...)
    syscall("process_removeListener", ...)
end

function event.removeHandler(...)
    event.removeListener(...)
end