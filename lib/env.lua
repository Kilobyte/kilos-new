env = {}

function env.getVar(name)
    local v = syscall("env_getglob", name)
    if v then return v end
    return syscall("env_getproc", name)
end