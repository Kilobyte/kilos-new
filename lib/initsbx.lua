--[[
    This file gets loaded into sandbox after creation
]]
local loadedlibs = {}

function error(msg, level)
    if level then level = level + 1 else level = 1 end
    syscall("lua_error", msg, level)
end

function include(lib)
    --syscall("print", lib)
    if loadedlibs[lib] then return end
    loadedlibs[lib] = true
    local f, m = syscall("vfs_open", "/lib/"..lib..".lua")
    if not f then error(m or "Some error on opening file ".."/lib/"..lib..".lua") end
    --syscall("print", f or "nil")
    local fn, err = syscall("lua_loadstring", syscall("vfs_readAll", f), "lib/"..lib)
    syscall("vfs_close", f)
    if err then error(err, 2) end
    fn()
end

syscall("env_setproc", "path", "/bin:/usr/bin:/sbin:/usr/sbin")
syscall("env_setproc", "libpath", "/lib:/usr/lib")
if not syscall("env_getproc", "dir") then
    syscall("env_setproc", "dir", "/")
end