include "table"
include "string"
include "keys"
include "event"
include "colors"

term = {}
for k, v in ipairs(syscall("term_listfunc")) do
    local k_ = k
    term[v] = function(...)
        return syscall("term", v, ...)
    end
end

function write( sText )
    local w,h = term.getSize()      
    local x,y = term.getCursorPos()
    
    local nLinesPrinted = 0
    local function newLine()
        if y + 1 <= h then
            term.setCursorPos(1, y + 1)
        else
            term.setCursorPos(1, h)
            term.scroll(1)
        end
        x, y = term.getCursorPos()
        nLinesPrinted = nLinesPrinted + 1
    end
    
    -- Print the line with proper word wrapping
    while string.len(sText) > 0 do
        local whitespace = string.match( sText, "^[ \t]+" )
        if whitespace then
            -- Print whitespace
            term.write( whitespace )
            x,y = term.getCursorPos()
            sText = string.sub( sText, string.len(whitespace) + 1 )
        end
        
        local newline = string.match( sText, "^\n" )
        if newline then
            -- Print newlines
            newLine()
            sText = string.sub( sText, 2 )
        end
        
        local text = string.match( sText, "^[^ \t\n]+" )
        if text then
            sText = string.sub( sText, string.len(text) + 1 )
            if string.len(text) > w then
                -- Print a multiline word               
                while string.len( text ) > 0 do
                    if x > w then
                        newLine()
                    end
                    term.write( text )
                    text = string.sub( text, (w-x) + 2 )
                    x,y = term.getCursorPos()
                end
            else
                -- Print a word normally
                if x + string.len(text) - 1 > w then
                    newLine()
                end
                term.write( text )
                x,y = term.getCursorPos()
            end
        end
    end
    
    return nLinesPrinted
end

function print( ... )
    local nLinesPrinted = 0
    for n,v in ipairs( { ... } ) do
        nLinesPrinted = nLinesPrinted + write( tostring( v ) )
    end
    nLinesPrinted = nLinesPrinted + write( "\n" )
    return nLinesPrinted
end

function printError( ... )
    if term.isColour() then
        term.setTextColour( colours.red )
    end
    print( ... )
    term.setTextColour( colours.white )
end

function read( _sReplaceChar, _tHistory )
    term.setCursorBlink( true )

    local sLine = ""
    local nHistoryPos = nil
    local nPos = 0
    if _sReplaceChar then
        _sReplaceChar = string.sub( _sReplaceChar, 1, 1 )
    end
    
    local w, h = term.getSize()
    local sx, sy = term.getCursorPos()  
    
    local function redraw( _sCustomReplaceChar )
        local nScroll = 0
        if sx + nPos >= w then
            nScroll = (sx + nPos) - w
        end
            
        term.setCursorPos( sx, sy )
        local sReplace = _sCustomReplaceChar or _sReplaceChar
        if sReplace then
            term.write( string.rep(sReplace, string.len(sLine) - nScroll) )
        else
            term.write( string.sub( sLine, nScroll + 1 ) )
        end
        term.setCursorPos( sx + nPos - nScroll, sy )
    end
    
    --local sEvent, param = event.waitFor("event_term_char", "event_term_key")
    local function onChar(meta, param)
        sLine = string.sub( sLine, 1, nPos ) .. param .. string.sub( sLine, nPos + 1 )
        nPos = nPos + 1
        redraw()
    end
    local exit = false
    local function onKey(meta, param)
        if param == keys.enter then
            -- Enter
            exit = true
            
        elseif param == keys.left then
            -- Left
            if nPos > 0 then
                nPos = nPos - 1
                redraw()
            end
            
        elseif param == keys.right then
            -- Right                
            if nPos < string.len(sLine) then
                nPos = nPos + 1
                redraw()
            end
        
        elseif param == keys.up or param == keys.down then
            -- Up or down
            if _tHistory then
                redraw(" ");
                if param == keys.up then
                    -- Up
                    if nHistoryPos == nil then
                        if #_tHistory > 0 then
                            nHistoryPos = #_tHistory
                        end
                    elseif nHistoryPos > 1 then
                        nHistoryPos = nHistoryPos - 1
                    end
                else
                    -- Down
                    if nHistoryPos == #_tHistory then
                        nHistoryPos = nil
                    elseif nHistoryPos ~= nil then
                        nHistoryPos = nHistoryPos + 1
                    end                     
                end
                
                if nHistoryPos then
                    sLine = _tHistory[nHistoryPos]
                    nPos = string.len( sLine ) 
                else
                    sLine = ""
                    nPos = 0
                end
                redraw()
            end
        elseif param == keys.backspace then
            -- Backspace
            if nPos > 0 then
                redraw(" ");
                sLine = string.sub( sLine, 1, nPos - 1 ) .. string.sub( sLine, nPos + 1 )
                nPos = nPos - 1                 
                redraw()
            end
        elseif param == keys.home then
            -- Home
            nPos = 0
            redraw()        
        elseif param == keys.delete then
            if nPos < string.len(sLine) then
                redraw(" ");
                sLine = string.sub( sLine, 1, nPos ) .. string.sub( sLine, nPos + 2 )               
                redraw()
            end
        elseif param == keys["end"] then
            -- End
            nPos = string.len(sLine)
            redraw()
        end
    end
    event.addHandler("event_term_key", onKey)
    event.addHandler("event_term_char", onChar)
    
    while not exit do syscall("yield") end

    event.removeHandler("event_term_key", onKey)
    event.removeHandler("event_term_char", onChar)

    term.setCursorBlink( false )
    term.setCursorPos( w + 1, sy )
    print()
    
    return sLine
end