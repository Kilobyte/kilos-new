include "metatables"

---------- Class streams.OutputStream ----------

streams.OutputStream = {}

-- constructor
local function OutputStream_new()
    local t = setmetatable({}, streams.InputStream)
    return t
end

function streams.OutputStream:write(text)

end

function streams.OutputStream:writeLine(text)
    self:write(text.."\n")
end

function streams.OutputStream:close()

end

setmetatable(streams.OutputStream, {__index=streams.OutputStream, __call=OutputStream_new})

---------- End of class streams.OutputStream ----------

---------- Class streams.InputStream ----------

streams.InputStream = {}

-- constructor
local function InputStream_new()
    local t = setmetatable({}, streams.InputStream)
    return t
end
setmetatable(streams.InputStream, {__index=streams.InputStream, __call=InputStream_new})

function streams.InputStream:read()

end

function streams.InputStream:readLine()

end

function streams.InputStream:close()

end

---------- End of class streams.InputStream ----------
