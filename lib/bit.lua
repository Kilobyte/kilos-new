bit = {}

function bit.tobits(...)
    return syscall("bit_tobits", ...)
end

function bit.blshift(...)
    return syscall("bit_lshift", ...)
end

function bit.lshift(...) -- regular lua bit api compatibility :D
    bit.blshift(...)
end

function bit.brshift(...)
    return syscall("bit_rshift", ...)
end

function bit.rshift(...)
    bit.brshift(...)
end

function bit.bxor(...)
    return syscall("bit_xor", ...)
end

function bit.bor(...)
    return syscall("bit_or", ...)
end

function bit.band(...)
    return syscall("bit_and", ...)
end

function bit.bnot(...)
    return syscall("bit_not", ...)
end

function bit.tonum(...)
    return syscall("bit_tonum", ...)
end