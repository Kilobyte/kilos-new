local lib = {"len", "find", "gmatch", "byte", "dump", "reverse", "upper", "format", "rep", "lower", "sub", "gsub", "match", "char", "startsWith", "endsWith", "split"}

string = {}
for k, v in ipairs(lib) do
    local v_ = v
    string[v_] = function(...)
        return syscall("string_"..v_, ...)
    end
end 

function tostring(...)
    return syscall("lua_tostring", ...)
end

function tonumber(...)
    return syscall("lua_tonumber", ...)
end