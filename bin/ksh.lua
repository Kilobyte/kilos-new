--[[
    KiloShell
    Provides a basic user interface
]]

include "term"
include "user"
include "io"
include "lua"
include "process"
include "env"
include "pathutils"
include "string"
include "table"

local exit = false

for k, v in pairs(term) do write(k..", ") end print()
function exececuteLine(line)
    local stuff = string.split(line, " ")
    local filename = path.resolve(stuff[1])
    if not filename then 
        return false, "File not found"
    end
    local file = io.File(filename):open()
    local code = file:readAll()
    file:close()
    local s, m = loadstring(code, file:getName())
    if not s then return false, m end
    local p = process.Process(s)
    p:setEnv({dir = env.getVar("dir"), runpath = io.combine(filename, "..")})
    table.remove(stuff, 1)
    p:run(unpack(stuff))
end

while not exit do
    local l = read()
    local s, m = exececuteLine(l)
    if not s then printError(m) end
end