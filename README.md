# KilOS Kernel Repo

## What is KilOS

KilOS is a [ComputerCraft][1] custom Operating System. This is the latest rewrite. At this point it is FAR away from being stable since not even the Kernel is ready for productive use. You should not download it unless you either plan on helping us or are really hardcore

## KilOS Internals

KilOS is a bit stuctured like Linux however there are a lot of differences. It has a handler based event system and strictly seperated Kernel/Usermode. 

[1]: http://www.computercraft.info/