--[[function func(name, num)
    for a = 1, num do
        syscall("print", name)
        syscall("yield")
    end
end
syscall("process_create", func, "process1", 2)
syscall("process_create", func, "process2", 3)
syscall("process_create", func, "process3", 1)]]

--[[local function print(text)
    syscall("io_stdout", text.."\n")
    syscall("print", "actually printed in pid "..syscall("process_getCurrent"))
end

function teststuff()
    print("lol")
    --syscall("io_stdout", "lol".."\n")
    syscall("print", "printed in pid "..syscall("process_getCurrent"))

    syscall("yield")
    syscall("yield")
end

local pid, tid = syscall("process_create", teststuff)
--syscall("print", "pidcreated:", pid)
syscall("yield")
syscall("yield")
syscall("print","out:"..syscall("io_readstdout", pid, "line"))
--syscall("print", "read in pid "..syscall("process_getCurrent"))
--print("lol")]]
syscall("process_addListener", "event_term_char", function (meta, char)
    syscall("term", "write", char)
end)

while true do
    syscall("yield")
end